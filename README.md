# badw_pdftotext

## Desription
This tool converts a pdf to plain text. It uses nodejs and a third party library called [pdf2json](https://github.com/modesty/pdf2json).

## Install
Get this repository with:
```
$> git clone git@gitlab.lrz.de:badw-it/badw_pdftotext.git
```

Change into the directory with:
```
$> cd badw_pdftotext
```

If you don't have node, download the binaries here [https://nodejs.org/en/download/](https://nodejs.org/en/download/).

Install the dependencies (check the package.json for more information) with:
```
$> npm install
```

## Use

The program takes two arguments. The first is the source PDF, the second is the output filename.

Start the program with:
```
$> node extractor.js input.pdf output.txt
```
If you downloaded the binaries, then you call node and npm from the local resp. the downloaded and extracted folder.

That's it. Check the log files at badw_pdftotext/logs in case of an error.

## License
Licensed under the [Apache License Version 2.0.](http://www.apache.org/licenses/LICENSE-2.0)