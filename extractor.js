/*
 * Licensed under http://www.apache.org/licenses/LICENSE-2.0
 * Attribution notice: by Daniel Schwarz. © http://badw.de
 */

/**
 * BadwPdfExtractor - A program to extract text from a PDF file.
 * To run this program use the bash and call:
 * $> node extractor.js source.pdf destination.txt
 * 
 * The program calls the module's run-method, reads the <b>source.pdf</b> and
 * extract it to the <b>destination.txt</b>
 * @type module
 */
var BadwPdfExtractor = BadwPdfExtractor || (function (process) {
    var fs = require('fs');
    var path = require('path');
    var PDFParser = require('pdf2json');
    var ini = require('ini');
    var config = ini.parse(fs.readFileSync('./config.ini', 'utf-8'));

    /**
     * @type WriteStreamObject
     */
    var logger = {};

    /**
     * @type Object
     */
    var sourceAndDestination = {};

    /**
     * Initialize the logger as a StreamWriter
     * @returns {WriteStream}
     */
    var initLogger = function () {
        var logfilePath = path.join(config.log.folder, config.log.error_file);

        if (!fs.existsSync(config.log.folder)) {
            fs.mkdirSync(config.log.folder);
        }
        if (!fs.existsSync(logfilePath)) {
            fs.closeSync(fs.openSync(logfilePath, 'w'));
        }
        return fs.createWriteStream(logfilePath, {flags: 'a'});
    };

    var logMessage = function (message) {
        logger.write(new Date().toString() + "\n");
        logger.write(message + "\n**************\n");
    };

    var getSourceAndDestinationFile = function () {
        if (process.argv.length !== 4) {
            console.log("Error.\nCall the script like this:\n$> node " + __filename + " input.pdf output.txt\n");
            logMessage("Too many arguments. Call the script like this: $> node " + __filename + " input.pdf output.txt");
            return false;
        }
        var src = process.argv[2];
        var dest = process.argv[3];
        
        return {
            src: process.argv[2],
            dest: process.argv[3]
        };
    };

    var extractText = function () {
        if (sourceAndDestination) {
            var pdfParser = new PDFParser(this, 1);
            pdfParser.on("pdfParser_dataError", function (err) {
                logMessage(err);
            });
            pdfParser.on("pdfParser_dataReady", function (data) {
                fs.writeFile(sourceAndDestination.dest, pdfParser.getRawTextContent(), function(err){
                    console.log(err);
                });
            });
            pdfParser.loadPDF(sourceAndDestination.src);
        }
    };
    
    var clean = function () {
        logger.end();
    };

    var run = function () {
        /*
         * Initialize the variables, extract the text and clean filehandles.
         */
        logger = initLogger();
        sourceAndDestination = getSourceAndDestinationFile();
        extractText();
        clean();
    };

    return {
        run: run
    };

})(process);


BadwPdfExtractor.run();
